import Vue from 'vue';
import App from './App.vue';
import store from './store/';
import VModal from 'vue-js-modal';
import Vuelidate from 'vuelidate';

Vue.use(VModal);
Vue.use(Vuelidate);

Vue.config.productionTip = false;

Vue.filter('separateThousands', function (val) {
  return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
});

new Vue({
  store,
  render: h => h(App)
}).$mount('#app');
