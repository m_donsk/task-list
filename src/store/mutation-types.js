export const SET_TASKS = 'SET_TASKS';
export const SET_USERS = 'SET_USERS';
export const CREATE_TASK = 'CREATE_TASK';
export const DELETE_TASK = 'DELETE_TASK';
export const UPDATE_TASK = 'UPDATE_TASK';
