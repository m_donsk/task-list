import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import uuidv4 from 'uuid/v4';
import { SET_TASKS, SET_USERS, DELETE_TASK, UPDATE_TASK, CREATE_TASK } from './mutation-types';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    // The list of tasks received from a server
    tasks: [],

    // The list of tasks which are currently editing by the user.
    editableTasks: [],

    users: []
  },

  getters: {
    findUserById: state => id => state.users.find(user => user.id === id),

    findTaskById: state => id => state.editableTasks.find(task => task.id === id),
    
    getIndexOfTaskById: state => id => state.editableTasks.findIndex(task => task.id === id),

    tasksWithUsers (state, getters) {
      return state.editableTasks.map(task => {
        const user = getters.findUserById(task.user_id) || { id: null, name: 'No Name', price: 0 };

        return { ...task, user, price: task.time * user.price, userName: user.name };
      });
    },

    isDirty (state) {
      if (state.tasks.length !== state.editableTasks.length) {
        return true;
      }

      for (const key of state.tasks.keys()) {
        for (const property of Object.keys(state.tasks[key])) {
          if (state.tasks[key][property] !== state.editableTasks[key][property]) {
            return true;
          }
        }
      }

      return false;
    }
  },

  mutations: {
    [ SET_TASKS ] (state, tasks) {
      state.tasks = tasks.map(task => ({ ...task }));
      state.editableTasks = tasks.map(task => ({ ...task }));
    },

    [ SET_USERS ] (state, users) {
      state.users = users;
    },

    [ UPDATE_TASK ] (state, { index, task }) {
      Vue.set(state.editableTasks, index, {
        id: task.id,
        name: task.name,
        time: task.time,
        user_id: task.user_id
      });
    },

    [ DELETE_TASK ] (state, index) {
      state.editableTasks.splice(index, 1);
    },

    [ CREATE_TASK ] (state) {
      state.editableTasks.push({
        id: uuidv4(),
        name: '',
        time: 0,
        user_id: null
      });
    }
  },

  actions: {

    // Synchronous actions for working with the local state

    updateTask ({ commit, getters }, task) {
      const index = getters.getIndexOfTaskById(task.id);

      if (index > -1) {
        commit(UPDATE_TASK, { index, task });
      }
    },

    deleteTaskById ({ commit, getters }, taskId) {
      const index = getters.getIndexOfTaskById(taskId);

      if (index > -1) {
        commit(DELETE_TASK, index);
      }
    },

    // Async actons for API requests

    loadTasks ({ commit }) {
      return axios.get('/tasks.json').then(({ data }) => commit(SET_TASKS, data));
    },

    loadUsers ({ commit }) {
      return axios.get('/users.json').then(({ data }) => commit(SET_USERS, data));
    },

    saveTasks ({ commit, state }) {
      // @TODO: change it to POST request
      // axios.get('/saveTask.json', state.editableTasks)
      return axios.get('/saveTask.json').then(({ data }) => {
        if (data.status !== 'ok') {
          throw new Error('Invalid data');
        }

        commit(SET_TASKS, state.editableTasks);
      });
    }
  }
});
